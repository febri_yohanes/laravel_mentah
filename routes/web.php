<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\systemController;
use App\Http\Controllers\Auth\AuthController;
use Laravel\Socialite\Facades\Socialite;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', [systemController::class, 'login'])->name('login route');
Route::get('logout', [AuthController::class, 'logout'])->name('route logout');
Route::get('google/{id}', [AuthController::class, 'redirecttogoogle'])->name('login');
Route::get('callback', [AuthController::class, 'handleCallback'])->name('redirect callback');

Route::group(['middleware' => ['CheckLogin']], function () {
    Route::get('/', [systemController::class, 'index'])->name('dashboard');
});



// Route::get('/google/{id}', 'Auth\AuthController@redirectToProvider');
// Route::get('/', function () {
//     return view('welcome');
// });
