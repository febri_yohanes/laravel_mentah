<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_user', function (Blueprint $table) {
            $table->increments('id_user')->unique();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('title_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('number')->nullable();
            $table->string('dob')->nullable();
            $table->string('occupation')->nullable();
            $table->string('foi')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address')->nullable();
            $table->string('salt')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('institution')->nullable();
            $table->string('phonework')->nullable();
            $table->string('faxwork')->nullable();
            $table->string('emailwork')->nullable();
            $table->integer('member_type')->nullable();
            $table->integer('membership_expiry')->nullable();
            $table->integer('reg_number')->nullable();
            $table->string('email_confirmation')->nullable();
            $table->string('ZIP')->nullable();
            $table->string('activation_code')->nullable();
            $table->string('forgotten_password_code')->nullable();
            $table->string('forgotten_password_time')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('remember_code')->nullable();
            $table->integer('active')->nullable();
            $table->timestamp('active_date')->nullable();
            $table->rememberToken()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
