<?php

namespace App\Http\Controllers\Auth;

use App\Models\User\m_user;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{

    public function redirecttogoogle($id)
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $email = $user->getEmail();
        $check_email = m_user::check_user($email);

        if ($check_email >= 1) {
            $data = m_user::get_user($email);
            // jika status tidak aktif maka di kembalikan kehalaman login
            if($data->active != 1){
                return redirect('/login');
            }else{
                Session::put('email', $email);
                Session::put('role', $data->role);
                Session::put('Status_active', 1);
                Session::put('login', '1');
                Session::put('keterangan', 'Pengguna');
                Session::put('data_email', $user);

                return redirect('/');
            }

        } else {
            Session::put('email', $email);
            Session::put('role', 1);
            Session::put('Status_active', 1);
            Session::put('login', '1');
            Session::put('keterangan', 'Pengguna');
            Session::put('data_email', $user);

            //jika email pendaftar tidak ditemukan.
            $new_user = m_user::create([
                'email' => $email,
                'active' => 1,
                'role' => 1,
            ]);

            return redirect('/');
        }
    }
    public function logout()
    {

        session_start();
        $_session['all'] = session::all();
        unset($_session['all']);
        $_session['all'] = session()->flush();


        return redirect('/');
        // return Redirect::to("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=".url('/redirect_homepage')."");

    }
}
