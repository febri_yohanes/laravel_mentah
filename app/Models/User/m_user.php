<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class m_user extends Model
{
    use HasFactory;
    protected $table = "m_user";
    protected $primaryKey = 'id_user';
    protected $casts = [
        'id_user' => 'string'
    ];
    protected $guarded = [];


    public static function check_user($id){
        $data=DB::table("m_user")
        ->select('*')
        ->where('email',$id)
        ->count();
        return $data;

    }

    public static function get_user($id){
        $data=DB::table("m_user")
        ->select('*')
        ->where('email',$id)
        ->first();
        return $data;

    }
}
