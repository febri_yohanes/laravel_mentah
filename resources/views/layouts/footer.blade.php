
    <!-- Core -->
    <script src="{{ asset('/template/vendor/@popperjs/core/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    <!-- Vendor JS -->
    <script src="{{ asset('/template/vendor/onscreen/dist/on-screen.umd.min.js') }}"></script>
    
    <!-- Slider -->
    <script src="{{ asset('/template/vendor/nouislider/dist/nouislider.min.js') }}"></script>
    
    <!-- Smooth scroll -->
    <script src="{{ asset('/template/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js') }}"></script>
    
    <!-- Charts -->
    <script src="{{ asset('/template/vendor/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('/template/vendor/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
    
    <!-- Datepicker -->
    <script src="{{ asset('/template/vendor/vanillajs-datepicker/dist/js/datepicker.min.js') }}"></script>
    
    <!-- Sweet Alerts 2 -->
    <script src="{{ asset('/template/vendor/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    
    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
    
    <!-- Vanilla JS Datepicker -->
    <script src="{{ asset('/template/vendor/vanillajs-datepicker/dist/js/datepicker.min.js') }}"></script>
    
    <!-- Notyf -->
    <script src="{{ asset('/template/vendor/notyf/notyf.min.js') }}"></script>
    
    <!-- Simplebar -->
    <script src="{{ asset('/template/vendor/simplebar/dist/simplebar.min.js') }}"></script>
    
    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    
    <!-- Volt JS -->
    <script src="{{ asset('/template/assets/js/volt.js') }}"></script>
    
        